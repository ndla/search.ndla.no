<!DOCTYPE HTML>
<html class="<?php print $modernizr_head; ?>" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf->namespaces; ?>>
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <link rel="profile" href="<?php print $rdf->profile; ?>">
  <?php print $head_scripts; ?>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" type="text/css">
  <?php print $styles; ?>
</head>
<body class="<?php print $classes; ?>" role="document" <?php print $attributes;?>>
  <div id="skip">
    <a href="#navigation"><?php print t('Jump to Navigation'); ?></a>
    <a href="#content"><?php print t('Jump to Content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php print $scripts; ?>
</body>
</html>
