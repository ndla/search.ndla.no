(function($) {
  var selected_facets_url = [];
  $(document).ready(function() {
    $('.btn.search').click(function() {
      if(selected_facets_url.length > 1) {
        selected_facets_url.push('concat=or');
      }

      if(window.location.search == '') {
        window.location = window.location.pathname + '?' + selected_facets_url.join('&');
      } else {
        window.location = window.location.toString() + '&' + selected_facets_url.join('&');
      }
    });

    $('.ndla_search-field-selector').click(function() {
      var url = '';
      if($(this).attr('name') == 'site') {
        url = $(this).attr('name') + '[]' + '=' + $(this).val();
      } else {
        url = 'f[' + $(this).attr('name') + '][]' + '=' + $(this).val();
      }
      if($(this).is(':checked')) {
        selected_facets_url.push(url);
      } else {
        selected_facets_url.splice(selected_facets_url.indexOf(url),1);
      }
    });

  });
}(jQuery))
