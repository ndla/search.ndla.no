(function($, Modernizr, Drupal) {
	if(typeof Modernizr !== "undefined") {
		// Cross browser html5 placeholders
		if(!Modernizr.input.placeholder){
			$('[placeholder]').focus(function() {
				var input = $(this);
				if (input.val() === input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = $(this);
				if (input.val() === '' || input.val() === input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}
			}).blur();
			$('[placeholder]').parents('form').submit(function() {
				$(this).find('[placeholder]').each(function() {
					var input = $(this);
					if (input.val() === input.attr('placeholder')) {
						input.val('');
					}
				});
			});
		}
	}

	$(document).ready(function () {
		// Language selector magic
		var menu = $('.language-switcher-locale-url'),
		active = menu.find('.active a').html();
		menu.prepend('<span class="lang-selector">'+active+' <i class="icon-angle-down"></i></span>');
		$('li', menu).wrapAll('<ul class="submenu element-hidden" />');

		$('.lang-selector').click(function () {
			$('.submenu', menu).toggleClass('element-hidden');
		});
	});
})(jQuery, Modernizr, Drupal);

/**
 * Performs a cached and delayed search.
 */
if (Drupal.ACDB) {
Drupal.ACDB.prototype.search = function (searchString) {
  var db = this;
  this.searchString = searchString;

  // See if this string needs to be searched for anyway.
  searchString = searchString.replace(/^\s+|\s+$/, '');
  if (searchString.length <= 0 ||
    searchString.charAt(searchString.length - 1) == ',') {
    return;
  }

  // See if this key has been searched for before.
  if (this.cache[searchString]) {
    return this.owner.found(this.cache[searchString]);
  }

  // Initiate delayed search.
  if (this.timer) {
    clearTimeout(this.timer);
  }
  this.timer = setTimeout(function () {
    db.owner.setStatus('begin');

    // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
    // encodeURIComponent to allow autocomplete search terms to contain slashes.
    jQuery.ajax({
      type: 'GET',
      url: db.uri + '/' + Drupal.encodePath(searchString),
      dataType: 'json',
      success: function (matches) {
        if (typeof matches.status == 'undefined' || matches.status != 0) {
          db.cache[searchString] = matches;
          // Verify if these are still the matches the user wants to see.
          if (db.searchString == searchString) {
            db.owner.found(matches);
          }
          db.owner.setStatus('found');
        }
      },
      error: function (xmlhttp) {
        if (xmlhttp.status != 0) {
          alert(Drupal.ajaxError(xmlhttp, db.uri));
        }
      }
    });
  }, this.delay);
};
}
