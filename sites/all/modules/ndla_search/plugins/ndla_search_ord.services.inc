<?php
/**
 * @file
 *  Services implementations for NDLA Search Ord
 * @author
 *  jonas.seffel@cerpus.com
 */

function ndla_search_ord_services_resources() {
  $topics_resource = array(
    'keywords' => array(
        'delete' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_ord'),
          'callback' => 'ndla_search_ord_delete_keywords_index',
          'help' => 'Wipe the Solr index for Topics or single document if argument is supplied.',
          'access callback' => 'user_access',
          'access arguments' => array('delete keywords index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'id',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The ID to delete',
            ),
          ),
        ),
        'create' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_ord'),
          'callback' => '_ndla_search_ord_update_keywords_index',
          'help' => 'Create or update the Solr index for Topics',
          'access callback' => 'user_access',
          'access arguments' => array('update keywords index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'args',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'string',
              'description' => 'The arguments',
            ),
          ),
        ),
    ),
    'topics' => array(
        'delete' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_ord'),
          'callback' => 'ndla_search_ord_delete_topics_index',
          'help' => 'Wipe the Solr index for Topics or single document if argument is supplied.',
          'access callback' => 'user_access',
          'access arguments' => array('delete topics index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'id',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The ID to delete',
            ),
          ),
        ),
        'create' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_ord'),
          'callback' => '_ndla_search_ord_update_topics_index',
          'help' => 'Create or update the Solr index for Topics',
          'access callback' => 'user_access',
          'access arguments' => array('update topics index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'args',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'string',
              'description' => 'The arguments',
            ),
          ),
        ),
    ),
  );
  
  return $topics_resource;
}

/** 
 * Function for updating the topics index. Almost the same as keywords index but differs slightly.
 */
function _ndla_search_ord_update_topics_index($args = NULL) {
  //Get Solr
  $topics = ndla_search_get_host('topics');
  $solr = ndla_search_get_solr($topics->host, TRUE);
  $dont_delete = !empty($args['dont_delete']) ? 1 : 0;

  if(!$solr || !$solr->ping()) {
    return array('success' => FALSE, 'message' => 'Unable to contact the index "' . $index . "'");
  }
  
  if(empty($args['keywords'])) {
    return array('success' => FALSE, 'message' => 'Expected variable keywords, got something else.');
  }

  $decoded = json_decode($args['keywords']);
  
  if(empty($decoded)) {
    return array('success' => FALSE, 'message' => 'Decoded JSON-string was empty');
  }

  $words = ndla_search_ord_topic_json_to_solr($decoded);

  if(empty($words)) {
    return array('success' => FALSE, 'message' => 'Nothing to index or data misinterpreted. Go read some code.');
  }

  $docs = array();
  $deleted = array();

  foreach($words as $index => $word) {
    if(!empty($word->data)) {
      $doc = new Apache_Solr_Document();
      $doc->id = $doc->docid = $word->id;
      if(!empty($word->type_id)) {
        foreach($word->type_id as $type_id) {
          $doc->addField('type_id', $type_id);
        }
      }
      //Beacuse Keywords may be splitted in several solr-documents - delete them all before updating the keyword.
      if(!$dont_delete) {
        $deleted[] = $doc->id;
      }
      $doc->docid = $doc->id . "-" . $word->wordclass;
      $doc->approved = $word->approved;
      $doc->visible = $word->visible;
      $doc->wordclass = $word->wordclass;

      if(!empty($word->approval_date)) {
        $doc->approval_date = $word->approval_date;
      }
      
      foreach($word->data as $field_name => $field_value) {
        $exact_title = str_replace("title", "exact_title", $field_name);
        $doc->$exact_title = $field_value;
        $doc->$field_name = $field_value;
        $doc->{'f_'.$field_name} = $field_value;
        $doc->{'b_'.$field_name} = $field_value;
      }
      
      if(!empty($doc->title_language_neutral)) { 
        $doc->title = @$word->data->title_und;
        $doc->f_title = @$word->data->title_und;
        $doc->b_title = @$word->data->title_und;
        $doc->exact_title = $doc->title;
      }
      
      if(empty($doc->title) && !empty($doc->title_eng)) {
        $doc->title = $doc->title_eng;
        $doc->f_title = $doc->title_eng;
        $doc->b_title = $doc->title_eng;
        $doc->exact_title = $doc->title;
      }
      
      foreach($word->descriptions as $field => $value) {
        $doc->$field = $value;
      }
      
      foreach($word->images as $field => $value) {
        $doc->$field = $value;
      }
      
      if(!empty($word->process_state)) {
        $doc->process_state = $word->process_state;
      }
      else {
        $doc->process_state = 0;
      }
      
      $docs[] = $doc;
    }
  }
  
  try {
    if(count($deleted)) {
      foreach($deleted as $did) {
        $solr->deleteById($did);
      }
      //$solr->commit();
    }
    $solr->addDocuments($docs);
    //$solr->commit();
    return array('success' => TRUE, 'message' => count($docs) . " document(s) added/updated.");
  }
  catch (Exception $e) {
    return array('success' => FALSE, 'message' => $e->getMessage());
  }
}

function _ndla_search_ord_update_keywords_index($args = NULL) {
  //Get Solr
  $topics = ndla_search_get_host('keywords');
  $solr = ndla_search_get_solr($topics->host, TRUE);
  $dont_delete = !empty($args['dont_delete']) ? 1 : 0;

  $is_keywords = ($index == 'keywords');

  if(!$solr || !$solr->ping()) {
    return array('success' => FALSE, 'message' => 'Unable to contact the index "' . $index . "'");
  }
  
  if(empty($args['keywords'])) {
    return array('success' => FALSE, 'message' => 'Expected variable keywords, got something else.');
  }

  $decoded = json_decode($args['keywords']);

  if(empty($decoded)) {
    return array('success' => FALSE, 'message' => 'Decoded JSON-string was empty');
  }
  
  $words = ndla_search_ord_json_to_solr($decoded);
  if(empty($words)) {
    return array('success' => FALSE, 'message' => 'Nothing to index or data misinterpreted. Go read some code.');
  }

  $docs = array();
  $deleted = array();

  foreach($words as $word) {
    if(!empty($word->data)) {
      foreach($word->data as $wordclass => $word_data) {
        $doc = new Apache_Solr_Document();
        $doc->id = $word->id;
        if(!empty($word->type_id)) {
          foreach($word->type_id as $type_id) {
            $doc->addField('type_id', $type_id);
          }
        }
        //Beacuse Keywords may be splitted in several solr-documents - delete them all before updating the keyword.
        if(!$dont_delete) {
          $deleted[] = $doc->id;
        }

        $doc->docid = $doc->id;
        $doc->docid = $doc->id . "-" . $wordclass;
        $doc->visible = $word->visible;
        
        foreach($word_data as $field_name => $field_value) {
          $exact_title = str_replace("title", "exact_title", $field_name);
          $doc->$exact_title = $field_value;
          $doc->$field_name = $field_value;
          $doc->{'f_'.$field_name} = $field_value;
          $doc->{'b_'.$field_name} = $field_value;
        }
        
        if(!empty($doc->title_language_neutral)) {
          $doc->exact_title = @$word_data->title_und;
          $doc->title = @$word_data->title_und;
          $doc->f_title = @$word_data->title_und;
          $doc->b_title = @$word_data->title_und;
        }
        
        if(empty($doc->title) && !empty($doc->title_eng)) {
          $doc->exact_title = $doc->title_eng;
          $doc->title = $doc->title_eng;
          $doc->f_title = $doc->title_eng;
          $doc->b_title = $doc->title_eng;
        }
        
        $doc->approved = $word->approved;
        if(!empty($word->approval_date)) {
          $doc->approval_date = $word->approval_date;
        }
        
        if(!empty($word->process_state)) {
          $doc->process_state = $word->process_state;
        }
        else {
          $doc->process_state = 0;
        }
        
        
        $doc->wordclass = $wordclass;
        $docs[] = $doc;
      }
    }
  }
  
  try {
    if(count($deleted)) {
      foreach($deleted as $did) {
        $solr->deleteById($did);
      }
      //$solr->commit();
    }
    $solr->addDocuments($docs);
    //$solr->commit();
    return array('success' => TRUE, 'message' => count($docs) . " document(s) added/updated.");
  }
  catch (Exception $e) {
    return array('success' => FALSE, 'message' => $e->getMessage());
  }
}

function ndla_search_ord_delete_topics_index($id) {
  return _ndla_search_ord_delete_index($id, 'topics');
}

function ndla_search_ord_delete_keywords_index($id) {
  return _ndla_search_ord_delete_index($id, 'keywords');
}

function _ndla_search_ord_delete_index($id = NULL, $index_selector = 'keywords') {
  $id = check_plain($id);
  
  //Get Solr
  $index = ndla_search_get_host($index_selector);
  $solr = ndla_search_get_solr($index->host, TRUE);

  if(!$solr || !$solr->ping()) {
    return array('success' => FALSE, 'message' => 'Unable to contact the index "' . $index . '"');
  }

  if(empty($id)) {
    return array('success' => FALSE, 'message' => 'No id specified');
  }

  //Wipe the complete index
  else if($id == 'all') {
    $solr->deleteByQuery('*:*');
    //$solr->commit();
    return array('success' => TRUE, 'message' => 'All documents deleted from the index');
  }
  //Remove single document
  else {
    $solr->deleteByQuery('id:' . $id);
    //$solr->commit();
    
    return array('success' => TRUE, 'message' => "If the document with id '$id' existed in the index '$index_selector' it is now deleted.");
  }
}
