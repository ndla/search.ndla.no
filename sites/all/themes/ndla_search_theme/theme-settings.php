<?php

// Form override fo theme settings
function ndla_search_theme_form_system_theme_settings_alter(&$form, $form_state) {

  $form['options_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Specific Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );  
  $form['options_settings']['ndla_search_theme_breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb settings'),
    '#attributes'    => array('id' => 'ndla_search_theme-breadcrumb'),
  );
  $form['options_settings']['ndla_search_theme_breadcrumb']['ndla_search_theme_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('ndla_search_theme_breadcrumb'),
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );
  $form['options_settings']['ndla_search_theme_breadcrumb']['ndla_search_theme_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => theme_get_setting('ndla_search_theme_breadcrumb_separator'),
    '#size'          => 5,
    '#maxlength'     => 10,
    '#prefix'        => '<div id="div-ndla_search_theme-breadcrumb-collapse">', // jquery hook to show/hide optional widgets
  );
  $form['options_settings']['ndla_search_theme_breadcrumb']['ndla_search_theme_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => theme_get_setting('ndla_search_theme_breadcrumb_home'),
  );
  $form['options_settings']['ndla_search_theme_breadcrumb']['ndla_search_theme_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('ndla_search_theme_breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['options_settings']['ndla_search_theme_breadcrumb']['ndla_search_theme_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('ndla_search_theme_breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
    '#suffix'        => '</div>', // #div-ndla_search_theme-breadcrumb
  );
  
  // Misc
  $form['options_settings']['ndla_search_theme_misc'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Misc'),
    '#attributes'    => array('id' => 'ndla_search_theme-misc'),
  );
  $form['options_settings']['ndla_search_theme_misc']['hide_title'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Hide title from frontpage.'),
    '#description'   =>t('This setting hides the title from the frontpage.'),
    '#default_value' => theme_get_setting('hide_title'),
  );

  // Additional js libs
  $form['options_settings']['ndla_search_theme_extra_js'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Javascript libraries'),
    '#attributes'    => array('id' => 'ndla_search_theme-extra-js'),
  );
  $form['options_settings']['ndla_search_theme_extra_js']['modernizr'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Add modernizr to the mix.'),
    '#description'   =>t('Adds modernizr.js to the mix. <a href="http://modernizr.com/" target="_blank">Read more about modernizr</a>'),
    '#default_value' => theme_get_setting('modernizr'),
  );
  $form['options_settings']['ndla_search_theme_extra_js']['hammer'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Add hammer to the mix.'),
    '#description'   =>t('Adds hammer.js to the mix. <a href="https://github.com/eightmedia/hammer.js" target="_blank">Read more about hammer</a>'),
    '#default_value' => theme_get_setting('hammer'),
  );

  // Search results
  $form['options_settings']['ndla_search_theme_search_result'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Search result page'),
    '#attributes'    => array('id' => 'ndla_search_theme-search-result'),
  );
  $form['options_settings']['ndla_search_theme_search_result']['ndla_search_theme_search_result_user'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Show author'),
    '#description'   => t(''),
    '#default_value' => theme_get_setting('ndla_search_theme_search_result_user'),
  );
  $form['options_settings']['ndla_search_theme_search_result']['ndla_search_theme_search_result_date'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Show created date'),
    '#description'   => t(''),
    '#default_value' => theme_get_setting('ndla_search_theme_search_result_date'),
  );
  $form['options_settings']['ndla_search_theme_search_result']['ndla_search_theme_search_result_extra'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Show extras'),
    '#description'   => t(''),
    '#default_value' => theme_get_setting('ndla_search_theme_search_result_extra'),
  );

  // Development
  $form['options_settings']['ndla_search_theme_development'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Development'),
    '#attributes'    => array('id' => 'ndla_search_theme-development'),
  );
  $form['options_settings']['ndla_search_theme_development']['wireframe_mode'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Wireframe Mode - Display borders around main layout elements'),
    '#description'   => t('<a href="!link">Wireframes</a> are useful when prototyping a website.', array('!link' => 'http://www.boxesandarrows.com/view/html_wireframes_and_prototypes_all_gain_and_no_pain')),
    '#default_value' => theme_get_setting('wireframe_mode'),
  );
  $form['options_settings']['ndla_search_theme_development']['clear_registry'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Rebuild theme registry on every page.'),
    '#description'   =>t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#default_value' => theme_get_setting('clear_registry'),
  );
  
  
}
