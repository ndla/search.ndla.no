<?php
/**
 * @file
 *  This contains all functionality for looking up data which is hosted by relate.
 */

function ndla_search_relate_get_aims($facets) {
  $uuids = array();
  $corrected_name = FALSE;
  $new_facets = array();
  //Pickup the UUID facets
  foreach($facets as $index => $facet) {
    $uuid = $facet['title'];
    $new_facets[$uuid] = $facet;
    if(strpos($uuid, "uuid:") === FALSE) {
      $uuid = "uuid:" . $uuid;
      $corrected_name = TRUE;
    }
    
    $uuids[] = $uuid;
  }
  
  //Look them up
  if(count($uuids)) {
    $result = xmlrpc(variable_get('ndla_search_relate_service_url', 'http://relate.test.ndla.no/services/xmlrpc'), array(
    'get.aim_names' => array($uuids, "true"),
    ));
    
    $result = json_decode($result);
    if($result) {
      $uuids_with_name = array();
      foreach($result as $id => $text) {
        $uuid = ($corrected_name) ? str_replace("uuid:", "", $id) : $id;
        if(empty($text)) {
          continue;
          //$text = empty($text) ? t('None provided') : $text;
        }
        
        $new_facets[$uuid]['title'] = $text;
      }
    
      return $new_facets;
    }
  }
  return $facets;
}

function ndla_search_uuid_to_name($uuids = array()) {
  global $language;
  $data = array();
  $nc = FALSE;
  foreach($uuids as $index => $uuid) {
    if(strpos($uuid, "uuid:") === FALSE) {
      $uuids[$index] = "uuid:" . $uuid;
      $nc = TRUE;
    }
  }
  
  if(count($uuids)) {
    $result = xmlrpc(variable_get('ndla_search_relate_service_url', 'http://relate.test.ndla.no/services/xmlrpc'), array(
    'get.aim_names' => array($uuids, "true", $language->language),
    ));
  
    if($result) {
      $result = json_decode($result);
      foreach($result as $index => $res) {
        if(empty($res)) {
          //Make debugging easier.
          $res = t('Failed lookup of uuid @uuid', array('@uuid' => $index));
        }
        if($nc) {
          $data[str_replace("uuid:", "", $index)] = $res;
        }
        else {
          $data[$index] = $res;
        }
      }
    }
  }
  
  return $data;
}