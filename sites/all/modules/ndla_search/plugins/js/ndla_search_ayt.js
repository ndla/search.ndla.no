var lastPhrase = "";
(function ($) {
  Drupal.behaviors.ayt = {
    attach: function (context, settings) {
      $('#ndla-search-search-form input', context).keyup(function (e) {
        searchString = $.trim($(this).val());
        setTimeout(function() {
          //Switch stolen (and slightly modified). Taken from misc/autocomplete.js
          switch (e.keyCode) {
            case 8: //Backspace
            case 16: // Shift.
            case 17: // Ctrl.
            case 18: // Alt.
            case 20: // Caps lock.
            case 32: //Space
            case 33: // Page up.
            case 34: // Page down.
            case 35: // End.
            case 36: // Home.
            case 37: // Left arrow.
            case 38: // Up arrow.
            case 39: // Right arrow.
            case 40: // Down arrow.
            case 9:  // Tab.
            case 13: // Enter.
            case 27: // Esc.
              return true;
          }

          if(lastPhrase != searchString) {
            lastPhrase = searchString;
            $.ajax({
              type: 'GET',
              url: Drupal.settings.ndla_search_ayt_result_path + '?keys=' + Drupal.encodePath(searchString),
              dataType: 'html',
              success: function (html) {
                $('.search-ayt').remove();
                $('#ndla-search-search-form').parents().first().append(html);
              },
              error: function (xmlhttp) {
              }
            });
          }
        }, 500); //setTimeout end.
      });
    }
  };
}(jQuery));
