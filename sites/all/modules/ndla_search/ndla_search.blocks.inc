<?php

function ndla_search_view_grep_ct_facet_block() {
  $fields = array('sm_ndla_grep_competence_aims_id');
  
  $theme_facets = array();
  $sites = ndla_search_get_searchable_sites(TRUE);
  $data = array();
  foreach($sites as $site) {
    $response = ndla_search_static_response_cache($site->id);
    foreach($fields as $field) {
      if(!empty($response->facet_counts->facet_fields->$field)) {
        foreach($response->facet_counts->facet_fields->$field as $facet => $count) {
          if(!ndla_search_is_facet_value_active('grep', $facet)) {
            $query = drupal_get_query_parameters();
            $query['f']['grep'][] = $facet;
            if(isset($theme_facets[$facet])) {
              $theme_facets[$facet]['count'] += $count;
            }
            else {
              $theme_facets[$facet] = array(
                'title' => NdlaGrepSearchClient::getAimNames($facet),
                'count' => intval($count),
                'query' => $query,
                'field' => 'grep',
                'facet' => $facet,
                'url' => url(current_path(), array('query' => $query)),
              );
            }
          }
        }
      }
    }
  }
  
  usort($theme_facets, function($a, $b) {
      return ($b['count'] - $a['count']);
  });
  
  if(!empty($theme_facets)) {
    $block['title'] = t('Competance aims');
    $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $theme_facets,
    );

    return $block;
  }
}
/**
 * Block implementations
 */

function ndla_search_view_language_facet_block() {
  $data = array();
  $name_to_lang = ndla_search_language_id_to_human();
  $names = ndla_search_get_language_ids();
  $facet_field = ndla_search_get_language_fields();

  $sites = array('ndla', 'fyrd7', 'deling', 'nygiv');

  foreach($facet_field as $site => $field) {
    $response = ndla_search_static_response_cache($site);
    if(!empty($response)) {
      if(isset($response->facet_counts->facet_fields->$field)) {
        foreach($response->facet_counts->facet_fields->$field as $facet => $count) {
          if(!ndla_search_is_facet_value_active('lang', $facet)) {
            $index = array_search($facet, $names[$site]);
            $data[$index] = empty($data[$index]) ? $count : $data[$index] + $count;
          }
        }
      }
    }
  }

  arsort($data);
  if(count($data)) {
    $links = array();
    $query = drupal_get_query_parameters();
    $theme_facets = array();
    foreach($data as $facet => $count) {
      $q_tmp = $query;
      $title = empty($name_to_lang[$facet]) ? $facet : $name_to_lang[$facet];
      $q_tmp['lang']['ndla'][] = $names['ndla'][$facet];
      $q_tmp['lang']['fyrd7'][] = $names['fyrd7'][$facet];

      $theme_facets[] = array(
        'title' => $title,
        'count' => $count,
        'query' => $q_tmp,
        //'multi_facet' => array('lang' => $q_tmp['lang']),
        'url' => url(current_path(), array('query'=>$q_tmp)),
      );
    }

    $block['title'] = t('Language');
    $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $theme_facets,
    );

    return $block;
  }
}

function ndla_search_view_drupal_content_type_facet_block() {
  global $language;

  $block = array();
  $facets = array();

  /**
   * Start NDLA
   */
  $host_id = 'ndla';
  $field = 'im_vid_100004';
  $host = ndla_search_get_host($host_id);
  $meta = ndla_search_get_facet_block($host);
  $response = ndla_search_static_response_cache($host_id);
  $taxonomy_facet_data = array();
  if($response) {
    $taxonomy_facet_data = ndla_search_get_taxonomy_facet_data($host, $field);
    foreach($response->facet_counts->facet_fields->$field as $facet => $count) {
      if(!ndla_search_is_facet_value_active($field, $facet)) {
        $is_child = ndla_search_term_is_child($facet, $taxonomy_facet_data);
        $term_data = !empty($taxonomy_facet_data[$facet]) ? $taxonomy_facet_data[$facet] : NULL;
        if(!$is_child) {
          $facets[$facet]['main'] = array(
            'facet' => $facet,
            'count' => $count,
            'title' => $term_data['title'],
            'field' => $field,
            'url' => _ndla_search_create_facet_url($field, $facet),
          );
        }
        else {
          $term_data = $taxonomy_facet_data[$is_child]['children'][$facet];
          $facets[$is_child]['children'][$facet] = array(
            'facet' => $facet,
            'count' => $count,
            'title' => $term_data['title'],
            'field' => $field,
            'url' => _ndla_search_create_facet_url($field, $facet),
          );
        }
      }
    }
  }
  /* Start of deling/fyr part */

  $sites = ndla_search_get_searchable_sites(TRUE);
  foreach($sites as $site) {
    $data = !empty($site->data) ? unserialize($site->data) : NULL;
    if($site->is_drupal && $site->id != 'ndla') {
      $response = ndla_search_static_response_cache($site->id);
      $type_fields = array('type', 'ss_type'); //type = D6, ss_type = D7
      foreach($type_fields as $type) {
        if($response && !empty($response->facet_counts->facet_fields->$type)) {
          foreach($response->facet_counts->facet_fields->$type as $facet => $count) {
            if(!ndla_search_is_facet_value_active($type, $facet)) {
              //Pickup the translated title
              if(!empty($data->content_types->$facet)) {
                $translation = !empty($data->content_types->$facet->{'title_'.$language->language}) ? $data->content_types->$facet->{'title_' . $language->language} : $data->content_types->$facet->title;
              }
              else {
                $translation = $facet;
              }

              if(!isset($facets[$facet]['main'])) {
                $facets[$facet]['main'] = array(
                  'facet' => $facet,
                  'count' => $count,
                  'title' => $translation,
                  'field' => $type,
                  'url' => _ndla_search_create_facet_url($type, $facet),
                );
              }
              else {
                $facets[$facet]['main']['count'] += $count;
              }

              $facets_sites[$facet][] = $site->site;
            }
          }
        } //end of $response
      }
    }
  }
  foreach($facets as $facet => $data) {
    if(!empty($facets_sites[$facet])) {
      $facets[$facet]['main']['title'] .= " (" . implode(" / ", $facets_sites[$facet]) . ")";
    }
  }
  /* Start sorting */

  usort($facets, 'ndla_search_facetsort');

  //Remove parents if there are active children
  foreach($facets as $tid => $data) {
    $children = (!empty($taxonomy_facet_data[$tid]['children'])) ? $taxonomy_facet_data[$tid]['children'] : array();
    foreach($children as $child_tid => $child_data) {
      if(empty($data['main'])) {
        continue;
      }
      if(ndla_search_is_facet_value_active($data['main']['field'], $child_tid)) {
        unset($facets[$tid]);
      }
    }
  }

  if(!empty($facets)) {
    $block = array(
      'subject' => t('Content type'),
      'content' => array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $facets,
        '#tree' => $taxonomy_facet_data,
      ),
    );
  }

  return $block;
}

function ndla_search_facetsort($a, $b) {
  if($a['main']['count'] >= $b['main']['count']) {
    return -1;
  }
  return 1;
}

/**
 * Sites facet block
 */
function ndla_search_view_sites_facet_block() {
  $query = drupal_get_query_parameters();
  $sites = ndla_search_get_searchable_sites(TRUE);
  $theme_facets = array();
  foreach($sites as $site) {
    $response = ndla_search_static_response_cache($site->id);
    if(!empty($response) && !ndla_search_is_facet_value_active('site', $site->id) && $response->response->numFound > 0) {
      $q_tmp = $query;
      $q_tmp['site'][] = $site->id;
      $theme_facets[] = array(
        'title' => $site->site,
        'count' => $response->response->numFound,
        'query' => $q_tmp,
        'url' => url(current_path(), array('query' => $q_tmp)),
        'facet' => $site->id,
        'field' => 'site',
      );
    }
  }

  if(count($theme_facets)) {
    $block = array();
    $block['title'] = t('Sites');
    $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $theme_facets,
    );
    return $block;
  }
}

/**
 * List all selected facets
 */
function ndla_search_view_active_search_block() {
  global $language;
  $block = $links = array();
  $searchable_sites = ndla_search_get_searchable_sites(TRUE);
  foreach($searchable_sites as $host) {
    $response = ndla_search_static_response_cache($host->id);
    if(!$response) {
      continue;
    }
    $total = (int) $response->response->numFound;
    $host = ndla_search_get_host($response->host_id);
    $query = drupal_get_query_parameters();
    unset($query['f']['grep']);
    //Keys are the only variable which we know of
    if(!empty($query['keys'])) {
      $keys = check_plain($query['keys']);
      $query_tmp = $query;
      unset($query_tmp['keys']);
      $links['keys'] = l(check_plain($keys), current_path(), array('query' => $query_tmp));
    }

    $params = array();
    $data = unserialize($host->data);
    drupal_alter('ndla_search_params', $params, $response->host_id);
    /* Enabled sites facet values */
    $enabled_site_facets = ndla_search_get_facet_value_for_site();
    if(count($enabled_site_facets)) {
      foreach($enabled_site_facets as $site) {
        if(!empty($searchable_sites[$site])) {
          $query_tmp = $query;
          $pos = array_search($site, $query['site']);
          unset($query_tmp['site'][$pos]);
          $links[$site] = l($searchable_sites[$site]->site, current_path(), array('query' => $query_tmp));
        }
      }
    }

    /* Other Facets */
    if(!empty($params['fq']) && !empty($query['f'])) {
      $relate_ids = array();
      $modified_params = _ndla_search_modify_active_search_params($params);
      foreach($modified_params['fq'] as $fq) {
        $filter = explode(":", $fq);
        if(!isset($data->facets->$filter[0])) {
          continue;
        }
        if(count($filter) == 2) {
          $vid = str_replace('im_vid_', '', $filter[0]);
          if(is_numeric($vid) && !empty($data->facets->_taxonomy->$vid->terms)) {
            $terms = $data->facets->_taxonomy->$vid->terms;
            foreach($terms as $term) {
              if($term->tid == $filter[1]) {
                $query_tmp = $query;
                $pos = array_search($filter[1], $query['f'][$filter[0]]);
                unset($query_tmp['f'][$filter[0]][$pos]);
                $links[] = l($term->name, current_path(), array('query' => $query_tmp));
              }
            }
          } else {
            $query_tmp = $query;
            $pos = array_search($filter[1], $query['f'][$filter[0]]);
            unset($query_tmp['f'][$filter[0]][$pos]);
            if($filter[0] == 'type' || $filter[0] == 'ss_type') {
              $title = $filter[1];
              if(!empty($data->content_types->$title)) {
                $tmp = $data->content_types->$title;
                $lang_field = 'title_' . $language->language;
                $title = !empty($tmp->lang_field) ? $tmp->lang_field : $tmp->title;
              }

              $links[$filter[1]] = l($title, current_path(), array('query' => $query_tmp));
            }
            else if(!empty($query['f'][$filter[0]])) {
              drupal_alter('ndla_search_active_filter', $filter);
              $filter[1] = str_replace('\ ', ' ', $filter[1]);
              $links[$filter[0]] = l($filter[1], current_path(), array('query' => $query_tmp));
            }
          }
        }
      }
    }
  }

  if(!empty($_GET['lang'])) {
    if(is_array($_GET['lang'])) {
      $sel_lang = array();
      $name_to_lang = ndla_search_language_id_to_human();
      $names = ndla_search_get_language_ids();

      foreach($_GET['lang'] as $site => $data) {
        if(is_array($data)) {
          foreach($data as $l) {
            $pos = array_search($l, $names[$site]);
            if($pos !== FALSE) {
              $sel_lang[$pos][$site] = $l;
            }
          }
        }
      }
      //One more loop
      foreach($sel_lang as $lang_prefix => $data) {
        $url_params = array();
        $q_tmp = $query;
        foreach($data as $site => $value) {
          $pos = array_search($value, $query['lang'][$site]);
          if($pos !== FALSE) {
            unset($q_tmp['lang'][$site][$pos]);
          }
        }

        $links[] = l(t($name_to_lang[$lang_prefix]), current_path(), array('query' => $q_tmp));
      }
    }
  }

  if(!empty($_GET['f']['grep'])) {
    $_GET['f']['grep'] = is_array($_GET['f']['grep']) ? $_GET['f']['grep'] : array($_GET['f']['grep']);
    foreach($_GET['f']['grep'] as $index => $aim) {
      $q_tmp = $query;
      unset($q_tmp['f']['grep'][$index]);
      $links[] = l(NdlaGrepSearchClient::getAimNames($aim), current_path(), array('query' => $q_tmp));
    }
  }

  if(count($links)) {
    drupal_add_css(drupal_get_path('module', 'ndla_search') . '/css/ndla_search_list.css', 'file');
    $block['title'] = t('My search');
    $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => $links,
        '#nobutton' => TRUE,
    );
  }

  return $block;
}

function _ndla_search_modify_active_search_params($params) {
  $new_params_fq = array();
  foreach($params['fq'] as $count => $fq) {
    list($field, $search_term) = explode(":", $fq);
    $ids = array();
    if(stripos($search_term, "OR") !== FALSE) {
      $ids = explode("OR", str_replace(array('(',')'), '', $search_term));
    }
    if(stripos($search_term, "AND") !== FALSE) {
      $ids = explode("AND", str_replace(array('(',')'), '', $search_term));
    }

    if(count($ids) == 0) {
      $ids[] = $search_term;
    }
    
    $search_terms = array_map('trim', $ids);
    foreach($search_terms as $search_term) {
      $new_params_fq[] = $field.':'.$search_term;
    }
  }
  $params['fq'] = $new_params_fq;
  return $params;
}

function ndla_search_ndla_search_active_filter_alter(&$filter) {
  $approved_solr_field_name = 'is_approved';

  if($filter[0] === $approved_solr_field_name) {
    $filter[1] = _ndla_search_approved_translations($filter[1]);
  }

}


function ndla_search_view_facet_block($delta) {
  $block = array();
  $facets = array();
  list($host_id, $field) = _ndla_search_parse_delta($delta);
  $host = ndla_search_get_host($host_id);

  if(!$host->enabled) {
    return;
  }
  $meta = ndla_search_get_facet_block($host);
  $response = ndla_search_static_response_cache($host_id);
  if(!$response) {
    return;
  }

  $tmp = explode("im_vid_", $field);
  //Taxonomy facets
  if(count($tmp) == 2) {
    $vid = $tmp[1];
    $taxonomy_facet_data = ndla_search_get_taxonomy_facet_data($host, $field);
    foreach($response->facet_counts->facet_fields->$field as $facet => $count) {
      if(!ndla_search_is_facet_value_active($field, $facet)) {
        $is_child = ndla_search_term_is_child($facet, $taxonomy_facet_data);
        $term_data = !empty($taxonomy_facet_data[$facet]) ? $taxonomy_facet_data[$facet] : NULL;
        if(!$is_child) {
          $facets[$facet]['main'] = array(
            'facet' => $facet,
            'count' => $count,
            'title' => $term_data['title'],
            'field' => $field,
            'url' => _ndla_search_create_facet_url($field, $facet),
          );
        }
        else {
          $term_data = $taxonomy_facet_data[$is_child]['children'][$facet];
          $facets[$is_child]['children'][$facet] = array(
            'facet' => $facet,
            'count' => $count,
            'title' => $term_data['title'],
            'field' => $field,
            'url' => _ndla_search_create_facet_url($field, $facet),
          );
        }
      }
    }


    //Remove parents if there are active children
    foreach($facets as $tid => $data) {
      $children = (!empty($taxonomy_facet_data[$tid]['children'])) ? $taxonomy_facet_data[$tid]['children'] : array();
      foreach($children as $child_tid => $child_data) {
        if(empty($data['main'])) {
          continue;
        }
        if(ndla_search_is_facet_value_active($data['main']['field'], $child_tid)) {
          unset($facets[$tid]);
        }
      }
    }

    if(!empty($facets)) {
      $block = array(
        'subject' => $meta[$delta]['title'],
      );
      $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $facets,
        '#tree' => $taxonomy_facet_data,
      );
    }
  }
  //Other fields
  else {
    //Lets go out and fetch SolR data
    $facets = array();
    foreach($response->facet_counts->facet_fields->$field as $facet => $count) {
      if(!ndla_search_is_facet_value_active($field, $facet)) {
        if($facet == '_empty_') {
          continue;
        }
        $title = $facet;
        $facets[$delta][] = array(
          'facet' => $facet,
          'count' => $count,
          'title' => $title,
          'field' => $field,
          'url' => _ndla_search_create_facet_url($field, $facet),
        );
      }
    }

    if(!empty($facets[$delta])) {
      drupal_alter('ndla_search_facet_title', $delta, $facets[$delta]);

      $block = array();
      $block['subject'] = $meta[$delta]['title'];

      $block['content'] = array(
        '#theme' => 'ndla_search_facet_item_list',
        '#items' => array(),
        '#facets' => $facets[$delta],
      );

    }
  }

  return $block;
}

/**
 * @param $host
 *  The complete host object
 */
function ndla_search_get_facet_block($host) {
  $blocks = array();
  $data = !empty($host->data) ? unserialize($host->data) : array();
  if(!empty($data->facets)) {
    foreach ($data->facets as $index => $facet) {
      if($index == '_taxonomy') {
        continue;
      }
      $info = (array)$facet->info;
      $info = array_pop($info);
      $facet_field = (array)$facet->facet_field;
      $facet_field = array_pop($facet_field);
      $block_title = $facet->block_title;
      $blocks[$host->id . "_f_" . $facet_field] = array(
        'info' => t('NDLA Search (facet for @host):', array('@host' => $host->site)) . $info,
        'title' => $facet->block_title,
        'field' => $facet_field,
      );
    }
  }
  
  return $blocks;
}

function ndla_search_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];
  if(variable_get($block->delta . '_block_collapsible', 0) == 1) {
    $vars['classes_array'][] = 'collapsible';
  }

  if(variable_get($block->delta . '_block_collapsible_default', 0) == 1) {
    $vars['classes_array'][] = 'collapsible_default';
  }
}
