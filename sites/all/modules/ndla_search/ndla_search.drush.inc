<?php


function ndla_search_drush_command() {
  $items['ndla_search-health'] = array(
    'description' => dt('NDLA Solr Health Checker'),
    'arguments' => array(),
  );
  return $items;
}

function drush_ndla_search_health() {
    $searchable_hosts = ndla_search_get_searchable_sites(TRUE);
    
    drush_log('Found ' .count($searchable_hosts). ' solr instances configured', 'ok');
    foreach($searchable_hosts as $host) {
        print "\n";
        drush_log($host->host, 'ok');

        $ping = ndla_search_ping_url($host->host);
        if($ping) {
            drush_log('Ping OK', 'ok');
        } else {
            drush_log('Ping failed', 'error');
            continue;
        }

        $query = array();
        
        $data = unserialize($host->data);
        
        $params = ndla_search_get_params_from_config($data);
        
        $response = ndla_search_search($host->host, $query, 0, 1, $params, $host->id, TRUE);

        drush_log('Solr documents found ' . $response->response->numFound, 'ok');

        $data = !empty($host->data) ? unserialize($host->data) : NULL;

        drush_log("\nPrinting {$host->name} facets and counts", "ok");
        foreach($response->facet_counts->facet_fields as $field_name => $facet) {
            $facet = (array)$facet;
            drush_log($field_name . ' (count: ' . count($facet) . ')' , "ok");

            if(count($facet) == 1) {
                drush_log("Only found 1 count for facet " . $field_name, "warning");
                var_dump($facet);
            }
            
        }
    }
}





















