<?php
/**
 * @file
 *  Services implementations for NDLA Search Ord
 * @author
 *  jonas.seffel@cerpus.com
 */

function ndla_search_grep_services_resources() {
  $grep_resource = array(
    'grep' => array(
        'delete' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_grep'),
          'callback' => '_ndla_search_grep_delete_index',
          'help' => 'Wipe the Solr index for grep or single document if argument is supplied.',
          'access callback' => 'user_access',
          'access arguments' => array('delete grep index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'id',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The ID to delete',
            ),
          ),
        ),
        'create' => array(
          'file' => array('type' => 'inc', 'module' => 'ndla_search_grep'),
          'callback' => '_ndla_search_grep_update_index',
          'help' => 'Create or update the Solr index for grep',
          'access callback' => 'user_access',
          'access arguments' => array('update grep index'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'args',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'string',
              'description' => 'The arguments',
            ),
          ),
        ),
    ),
  );
  
  return $grep_resource;
}

function _ndla_search_grep_update_index($args = NULL) {
  //Get Solr
  $grep = ndla_search_get_host('grep');
  $solr = ndla_search_get_solr($grep->host, TRUE);
  $dont_delete = !empty($args['dont_delete']) ? 1 : 0;

  if(!$solr || !$solr->ping()) {
    return array('success' => FALSE, 'message' => 'Unable to contact the grep index');
  }
  
  if(empty($args['grep'])) {
    return array('success' => FALSE, 'message' => 'Expected variable grep, got something else.');
  }

  $decoded = json_decode($args['grep']);
  
  if(empty($decoded)) {
    return array('success' => FALSE, 'message' => 'Decoded JSON-string was empty');
  }
  
  $words = ndla_search_grep_json_to_solr($decoded);

  if(empty($words)) {
    return array('success' => FALSE, 'message' => 'Nothing to index or data misinterpreted. Go read some code.');
  }

  $docs = array();
  $deleted = array();
  foreach($words as $grep) {
    $doc = new Apache_Solr_Document();
    $doc->id = $grep->id;
    $doc->type = $grep->type;
  
    foreach($grep->data as $field_name => $field_value) {
      $doc->$field_name = $field_value;
    }
  
    $docs[] = $doc;
  }
  
  try {
    $solr->addDocuments($docs);
    $solr->commit();
    return array('success' => TRUE, 'message' => count($docs) . " document(s) added/updated.");
  }
  catch (Exception $e) {
    return array('success' => FALSE, 'message' => $e->getMessage());
  }
}

function _ndla_search_grep_delete_index($id = NULL) {
  $id = check_plain($id);

  //Get Solr
  $grep = ndla_search_get_host('grep');
  $solr = ndla_search_get_solr($grep->host, TRUE);

  if(!$solr || !$solr->ping()) {
    return array('success' => FALSE, 'message' => 'Unable to contact the grep index');
  }

  if(empty($id)) {
    return array('success' => FALSE, 'message' => 'No id specified');
  }

  //Wipe the complete index
  else if($id == 'all') {
    $solr->deleteByQuery('*:*');
    $solr->commit();
    return array('success' => TRUE, 'message' => 'All documents deleted from the index');
  }
  //Remove single document
  else {
    //$solr->deleteById($id);
    $solr->deleteByQuery('id:' . $id);
    $solr->commit();
    return array('success' => TRUE, 'message' => "If the document with id '$id' existed, it is no longer in the index.");
  }
}
