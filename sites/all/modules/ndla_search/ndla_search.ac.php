<?php

/**
 * This file creates a lightweight bootstrap and calls the autocomplete callback.
 */

chdir('../../../../');
define('DRUPAL_ROOT', getcwd());
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";

$bootstraps = array(
//  DRUPAL_BOOTSTRAP_CONFIGURATION,
//  DRUPAL_BOOTSTRAP_PAGE_CACHE,
//  DRUPAL_BOOTSTRAP_DATABASE,
  DRUPAL_BOOTSTRAP_VARIABLES,
  DRUPAL_BOOTSTRAP_SESSION,
//  DRUPAL_BOOTSTRAP_PAGE_HEADER,
  DRUPAL_BOOTSTRAP_LANGUAGE,
  
);
foreach($bootstraps as $bootstrap) {
  drupal_bootstrap($bootstrap);
}

require_once('sites/all/modules/ndla_search/ndla_search.module');

ndla_search_init();
ndla_search_autocomplete(arg(1), arg(2));