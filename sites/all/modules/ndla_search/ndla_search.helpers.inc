<?php
/**
 * Helper functions for parsing and fetching various data.
 */
 
function ndla_search_get_language_fields() {
  return array(
    'ndla' =>'language', 
    'fyrd7' => 'ss_language',
    'deling' => 'ss_language', 
  );
}

function ndla_search_get_taxonomy_facet_data($host, $field) {
  global $language;
  $term_links = array();
  $data = unserialize($host->data);
  $vid = str_replace('im_vid_', '', $field);
  $terms = new stdClass();


  if(is_numeric($vid) && !empty($data->facets->_taxonomy->$vid->terms)) {
    $terms = $data->facets->_taxonomy->$vid->terms;
  }

  foreach($terms as $term) {
    $parents = array_filter((array)$term->parents);
    $new_term = array(
      'tid' => $term->tid,
      'vid' => $term->vid,
      'title' => !empty($term->title) ? $term->title : '',
      'depth' => $term->depth,
    );


    if(isset($language)) {
      $title_lang = 'title_'.$language->language;
      if(!empty($term->$title_lang)) {
        $new_term['title'] = $term->$title_lang;
      }
    }
    
    if(empty($data->facets->_taxonomy->$vid->flat_structure) && count($parents)) {
      foreach($parents as $parent) {
        $term_links[$parent]['children'][$term->tid] = $new_term;
      }
    }
    else {
      $term_links[$term->tid] = $new_term;
    }
  }

  return $term_links;
}

function _ndla_search_parse_delta($delta) {
  return explode("_f_", $delta);
}

function _ndla_search_name_to_id($name) {
  $name = str_replace(" ", "-", strtolower(trim($name)));
  return preg_replace("/[^a-z0-9-_]/", "", $name);
}

function ndla_search_get_language_ids() {
  $names = array(
    'deling' => array('neutral' => 'und', 'nn' => 'nn', 'nb' => 'nb', 'en' => 'en'),
    'fyrd7' => array('neutral' => 'und', 'nn' => 'nn', 'nb' => 'nb', 'en' => 'en'),
    'ndla' =>  array('neutral' => 'und', 'nn' => 'nn', 'nb' => 'nb', 'en' => 'en'),
    
  );
  
  return $names;
}

function ndla_search_language_id_to_human($id = NULL) {
  $l = array('nb' => 'Norwegian Bokmål', 'nn' => 'Norwegian Nynorsk', 'en' => 'English', 'neutral' => 'Neutral');
  if($id && !empty($l[$id])) {
    return $l['id'];
  }
  return $l;  
}