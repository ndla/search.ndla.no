<div class='sites'>
  <?php if(count($sites)): ?>
  <div class='site_header'>
    <span class='column'><?php print t('Name'); ?></span>
    <span class='column'><?php print t('Host'); ?></span>
    <span class='column tiny'><?php print t('Is Drupal'); ?></span>
    <span class='column tiny'><?php print t('Enabled'); ?></span>
    <span class='column tiny'></span>
    <span class='column tiny'></span>
    <div class='clearfix'></div>
  </div>
  <?php endif; $counter = 0; ?>
  
  <?php foreach($sites as $id => $site): ?>
  <div class='site <?php print ((++$counter % 2) == 0) ? 'even' : 'odd';?> '>
    <span class='column'><?php print $site->site; ?></span>
    <span class='column'><?php print $site->host; ?></span>
    <span class='column tiny'><?php print ($site->is_drupal) ? t('Yes') : t('No'); ?></span>
    <span class='column tiny'><?php print ($site->enabled) ? t('Yes') : t('No'); ?></span>
    <span class='column tiny'><?php print l(t('Edit'), 'admin/config/search/ndla_search/edit/' . $site->id); ?></span>
    <span class='column tiny'><?php print l(t('Delete'), 'admin/config/search/ndla_search/delete/' . $site->id); ?></span>
    <div class='clearfix'></div>
  </div>
  <?php endforeach; ?>
</div>