<?php

/**
 * This file creates a lightweight bootstrap and calls the autocomplete callback.
 */

chdir('../../../../../');
require_once('sites/all/modules/ndla_search/ndla_search.module');

//Avoid bootstrapping if request method is OPTIONS.
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  header($_SERVER['SERVER_PROTOCOL'] . ' 200');
  ndla_search_send_autocomplete_headers();
  exit();
}

define('DRUPAL_ROOT', getcwd());
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";

$bootstraps = array(
  DRUPAL_BOOTSTRAP_VARIABLES,
  DRUPAL_BOOTSTRAP_SESSION,
  DRUPAL_BOOTSTRAP_LANGUAGE,
);
foreach($bootstraps as $bootstrap) {
  drupal_bootstrap($bootstrap);
}

require_once('sites/all/modules/ndla_search/plugins/ndla_search_ord.module');
require_once('modules/locale/locale.module');
ndla_search_init();
ndla_search_ord_init();

$index_selector = (!empty($_REQUEST['index'])) ? check_plain($_REQUEST['index']) : 'keywords';
$rows = ndla_search_ord_autocomplete_search($index_selector);

exit();
