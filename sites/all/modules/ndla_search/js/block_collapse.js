(function($) {
  $(document).ready(function() {
    
    $('.block.collapsible').each(function() {

      var block = $(this);

      $('h2', block).append('<i class="icon-angle-up"></i>');

      $('h2', block).bind('click', function() {

        $(this).parent().toggleClass('collapsed');
        $(this).parent().find('.content').slideToggle(100);

        if ($('i', this).is('.icon-angle-up')) {
          $('i',this).attr('class', 'icon-angle-down');
        } else {
          $('i',this).attr('class', 'icon-angle-up');
        }

      });

      // Check if block is collapsed by default
      if (block.is('.collapsible_default')) {

        $('h2 i', block).attr('class', 'icon-angle-down');
        $(this).parent().addClass('collapsed');
        $('.content', this).hide();

      };

    });

  });
}(jQuery))