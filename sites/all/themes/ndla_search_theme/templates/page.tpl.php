
<header id="header" class="clearfix" role="banner">
  <div class="wrapper clearfix">

    <?php if ($logo): ?>
      <a href="<?php print url('https://ndla.no'); ?>" title="NDLA" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Link to https://ndla.no'); ?>"/>
      </a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan">

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>

      </div>
    <?php endif; ?>

    <?php if ($secondary_menu || render($page['topnavigation'])): ?>
      <nav id="top-navigation" role="navigation">
        <?php print render($page['topnavigation']); ?>
        <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('menu', 'top-menu', 'inline')))); ?>
      </nav>
    <?php endif; ?>

    <?php print render($page['header']); ?>

  </div>
</header> 

<?php if ($main_menu || render($page['navigation'])): ?>
  <nav id="navigation" class="clearfix" role="navigation">
    <?php if (render($page['navigation'])): ?>      
      <?php print render($page['navigation']); ?>
    <?php else: ?>
      <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => 'menu'))); ?>
    <?php endif; ?>    
  </nav>
<?php else: ?>
  <nav id="navigation" class="clearfix" role="navigation">&nbsp;</nav>
<?php endif; ?>


<div id="content" class="clearfix">
  <div class="wrapper clearfix">

    <section id="main" class="clearfix" role="main">

      <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>

        <?php print $breadcrumb; ?>
        
        <?php if ($page['billboard']): ?>
          <div id="billboard"><?php print render($page['billboard']) ?></div>
        <?php endif; ?>

        <?php if ($title): ?>
          <h1 class="<?php print $title_class ?>"><?php print $title; ?></h1>
        <?php endif; ?>

        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>

        <?php if ($tabs): ?>
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>

        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

      <?php endif; ?>

      <?php print render($page['content']) ?>

      <?php print $feed_icons; ?>

    </section>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>

  </div>
</div>

<?php print render($page['footer']); ?>


