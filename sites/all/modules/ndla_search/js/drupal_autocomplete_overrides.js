/**
 * Performs a cached and delayed search.
 */

var submit_event = false;

(function ($) {
  
Drupal.jsAC.prototype.setStatus = function (status) {
  if(!submit_event) {
    submit_event = true;
    jQuery('#edit-button').bind('click', function(ev) {
      setTimeout(function() {
        jQuery('#ndla-search-search-form').trigger('submit');
      }, 400)
    });
  }

  switch (status) {
    case 'begin':
      $(this.input).addClass('throbbing');
      $(this.ariaLive).html(Drupal.t('Searching for matches...'));
      break;
    case 'cancel':
    case 'error':
    case 'found':
      $(this.input).removeClass('throbbing');
      break;
  }
};
Drupal.ACDB.prototype.search = function (searchString) {
  var db = this;
  this.delay = 1;

  this.searchString = searchString;

  // See if this string needs to be searched for anyway.
  searchString = searchString.replace(/^\s+|\s+$/, '');
  if (searchString.length <= 0 ||
    searchString.charAt(searchString.length - 1) == ',') {
    return;
  }

  // See if this key has been searched for before.
  if (this.cache[searchString]) {
    return this.owner.found(this.cache[searchString]);
  }

  // Initiate delayed search.
  if (this.timer) {
    clearTimeout(this.timer);
  }

  this.timer = setTimeout(function () {
    db.owner.setStatus('begin');

    // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
    // encodeURIComponent to allow autocomplete search terms to contain slashes.
    $.ajax({
      type: 'GET',
      url: db.uri + '/' + Drupal.encodePath(searchString),
      dataType: 'json',
      success: function (matches) {
        if (typeof matches.status == 'undefined' || matches.status != 0) {
          db.cache[searchString] = matches;
          // Verify if these are still the matches the user wants to see.
          if (db.searchString == searchString) {
            db.owner.found(matches);
          }
          db.owner.setStatus('found');
        }
      }
    });
  }, this.delay);
};

Drupal.jsAC.prototype.select = function (node) {
  this.input.value = $(node).data('autocompleteValue');
  setTimeout(function() {
    jQuery('#ndla-search-search-form').trigger('submit');
  }, 400);
};

var old_onkeydown = Drupal.jsAC.prototype.onkeydown;
Drupal.jsAC.prototype.onkeydown = function(input, e) {
	// If user presses "enter" key.
	// Always submit form by simulating button click.
	if(e.keyCode === 13) {
		this.hidePopup(e.keyCode);
		$('.form-submit', $(input).parents('form').first()).click();
	}
	// apply (run) the old function otherwise
	old_onkeydown.apply(this, arguments);
};

})(jQuery);
