<div class='search-row'>
  <span class="row-title">
    <?php print $row['title'];?>
  </span>&nbsp;
  <span class="row-type"><?php print $row['type']; ?></span>&nbsp;
  
  <?php if(!empty($row['changed_at'])): ?>
    <span class="row-changedat"><?php print $row['changed_at']; ?></span>
  <?php endif; ?>
  
  <div class='snippet'>
    <?php print($row['snippet']);?>
  </div>
</div>