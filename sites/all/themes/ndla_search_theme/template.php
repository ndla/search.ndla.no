<?php

/**
 * Here we override the default HTML output of drupal.
 * refer to http://drupal.org/node/550722
 */
 
// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('clear_registry') && !defined('MAINTENANCE_MODE')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
  // Set warning message
  drupal_set_message(t('For easier theme development, the theme registry is being rebuilt on every page request. It is <em>extremely</em> important to <a href="!link">turn off this feature</a> on production websites.', array('!link' => url('admin/appearance/settings/' . $GLOBALS['theme']))), 'warning', FALSE);
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function ndla_search_theme_preprocess_html(&$vars) {

  // Add variables and paths needed for HTML5 and responsive support.
  $vars['base_path'] = base_path();
  $vars['path_to_ndla_search_theme'] = drupal_get_path('theme', 'ndla_search_theme');
  //$html5_respond_meta = theme_get_setting('ndla_search_theme_html5_respond_meta');

  // Attributes for html element.
  $vars['html_attributes_array'] = array(
    'lang' => $vars['language']->language,
    'dir' => $vars['language']->dir
  );

  // Send X-UA-Compatible HTTP header to force IE to use the most recent
  // rendering engine or use Chrome's frame rendering engine if available.
  // This also prevents the IE compatibility mode button to appear when using
  // conditional classes on the html tag.
  if (is_null(drupal_get_http_header('X-UA-Compatible'))) {
    drupal_add_http_header('X-UA-Compatible', 'IE=edge,chrome=1');
  }

  // Fix for RDF & html5.
  $prefixes = array();
  $namespaces = explode("\n", trim($vars['rdf_namespaces']));
  foreach ($namespaces as $name) {
    list($key,$url) = explode('=', $name, 2);
    list($xml,$space) = explode(':',$key, 2);
    $url = trim($url, '"');
    if (!empty($space) && !empty($url)) {
      $prefixes[] = $space . ': ' . $url;
    }
  }
  $prefix = implode(" ", $prefixes);
  
  $vars['rdf'] = new stdClass;
  //$vars['rdf']->version = 'XHTML+RDFa 1.1'; <-- Useless
  $vars['rdf']->namespaces = ' xmlns="https://www.w3.org/1999/xhtml" prefix="' . $prefix . '"';
  $vars['rdf']->profile = 'https://www.w3.org/TR/html5';

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  if (!$vars['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    $arg = explode('/', $_GET['q']);
    if ($arg[0] == 'node' && isset($arg[1])) {
      if ($arg[1] == 'add') {
        $section = 'node-add';
      }
      elseif (isset($arg[2]) && is_numeric($arg[1]) && ($arg[2] == 'edit' || $arg[2] == 'delete')) {
        $section = 'node-' . $arg[2];
      }
    }
    $vars['classes_array'][] = drupal_html_class('section-' . $section);
  }

  // Move JS files "$scripts" to page bottom for perfs/logic.
  // Add JS files that *needs* to be loaded in the head in a new "$head_scripts" scope.
  // For instance the Modernizr lib.

  // Add "no-js" class to <html> if Modernizr is included
  $vars['modernizr_head'] = '';

  if (theme_get_setting('modernizr')) {
    $vars['modernizr_head'] = 'no-js';
    drupal_add_js($vars['path_to_ndla_search_theme'] . '/js/libs/modernizr.min.js', array('scope' => 'head_scripts', 'weight' => -1, 'preprocess' => FALSE));  
  }

  // Hammer lib.
  if (theme_get_setting('hammer')) {
    drupal_add_js($vars['path_to_ndla_search_theme'] . '/js/libs/hammer.js', array('weight' => 0));  
    drupal_add_js($vars['path_to_ndla_search_theme'] . '/js/libs/jquery.hammer.js', array('weight' => 1));  
  }

  // Adds our custom scripts here, instead of .info, due to js aggregation.
  drupal_add_js($vars['path_to_ndla_search_theme'] . '/js/plugins.js', array('weight' => 49));  
  drupal_add_js($vars['path_to_ndla_search_theme'] . '/js/scripts.js', array('weight' => 50));  


  // Adds extra classes based on the used browser
  $browsername_and_version = _ndla_search_theme_identify_useragent($_SERVER['HTTP_USER_AGENT']);
  $ie_classes = _ndla_search_theme_create_ie_class_names($browsername_and_version);
  if(!empty($browsername_and_version['browser'])) {
    $vars['classes_array'][] = ndla_search_theme_id_safe($browsername_and_version['browser']);
  }

  if(!empty($browsername_and_version['version'])) {
    $vars['classes_array'][] = ndla_search_theme_id_safe($browsername_and_version['browser'] . $browsername_and_version['version']);
  }

  foreach($ie_classes as $classname) {
    $vars['classes_array'][] = ndla_search_theme_id_safe($classname);
  }
}

function ndla_search_theme_process_html(&$vars) {
  // Added scope for head scripts
  $vars['head_scripts'] = drupal_get_js('head_scripts');

  // Flatten out html_attributes.
  $vars['html_attributes'] = drupal_attributes($vars['html_attributes_array']);
}

function ndla_search_theme_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
  }
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function ndla_search_theme_preprocess_page(&$vars, $hook) {
  if (isset($vars['node_title'])) {
    $vars['title'] = $vars['node_title'];
    
  }

  // Hide title from frontpage
  $vars['title_class'] = 'page-title';

  if (theme_get_setting('hide_title') && drupal_is_front_page()) {
    $vars['title_class'] = "page-title element-hidden";
  }

  // Adding a class to #page in wireframe mode
  if (theme_get_setting('wireframe_mode')) {
    $vars['classes_array'][] = 'wireframe-mode';
  }

  // Add first/last classes to node listings about to be rendered.
  if (isset($vars['page']['content']['system_main']['nodes'])) {
    // All nids about to be loaded (without the #sorted attribute).
    $nids = element_children($vars['page']['content']['system_main']['nodes']);
    // Only add first/last classes if there is more than 1 node being rendered.
    if (count($nids) > 1) {
      $first_nid = reset($nids);
      $last_nid = end($nids);
      $first_node = $vars['page']['content']['system_main']['nodes'][$first_nid]['#node'];
      $first_node->classes_array = array('first');
      $last_node = $vars['page']['content']['system_main']['nodes'][$last_nid]['#node'];
      $last_node->classes_array = array('last');
    }
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function ndla_search_theme_preprocess_node(&$vars, $hook) {

  unset($vars['title_attributes_array']['datatype']);

  // Add $unpublished variable.
  $vars['unpublished'] = (!$vars['status']) ? TRUE : FALSE;

  // Add a striping class.
  $vars['classes_array'][] = 'node-' . $vars['zebra'];

  // Merge first/last class (from ndla_search_theme_preprocess_page) into classes array of current node object.
  $node = $vars['node'];
  if (!empty($node->classes_array)) {
    $vars['classes_array'] = array_merge($vars['classes_array'], $node->classes_array);
  }

  $vars['title_attributes_array']['class'][] = 'node-title';

}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function ndla_search_theme_preprocess_comment(&$vars, $hook) {
  // If comment subjects are disabled, don't display them.
  if (variable_get('comment_subject_field_' . $vars['node']->type, 1) == 0) {
    $vars['title'] = '';
  }

  // Add pubdate to submitted variable.
  $vars['pubdate'] = '<time pubdate datetime="' . format_date($vars['comment']->created, 'custom', 'c') . '">' . $vars['created'] . '</time>';
  $vars['submitted'] = t('!username replied on !datetime', array('!username' => $vars['author'], '!datetime' => $vars['pubdate']));

  // Zebra striping.
  if ($vars['id'] == 1) {
    $vars['classes_array'][] = 'first';
  }
  if ($vars['id'] == $vars['node']->comment_count) {
    $vars['classes_array'][] = 'last';
  }
  $vars['classes_array'][] = $vars['zebra'];

  $vars['title_attributes_array']['class'][] = 'comment-title';
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function ndla_search_theme_preprocess_block(&$vars, $hook) {

  // Use a template with no wrapper for the page's main content.
  if ($vars['block_html_id'] == 'block-system-main' || $vars['block_html_id'] == 'block-locale-language') {
    
    $vars['theme_hook_suggestions'][] = 'block__no_wrapper';

  }

  // Add a striping class.
  $vars['classes_array'][] = 'block-' . $vars['block_zebra'];

  // Classes describing the position of the block within the region.
  if ($vars['block_id'] == 1) {
    $vars['classes_array'][] = 'first';
  }
  // The last_in_region property is set in ndla_search_theme_page_alter().
  if (isset($vars['block']->last_in_region)) {
    $vars['classes_array'][] = 'last';
  }

  // Add WAI-ARIA roles to search block
  if ($vars['block']->module == 'search') {

    $vars['attributes_array']['role'] = 'search';
    
  }
}

/**
 * Override or insert variables into the menu-block wrapper templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function ndla_search_theme_preprocess_menu_block_wrapper(&$vars) {

  //print_r($vars);

}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function ndla_search_theme_process_block(&$vars, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $vars['title'] = $vars['block']->subject;
}

function ndla_search_theme_preprocess_entity(&$vars) {
  if (!empty($vars['classes_array']) && !empty($vars['attributes_array']['class'])) {    
    unset($vars['attributes_array']['class']);
  }
  if (empty($vars['attributes_array']['typeof'])) {
    unset($vars['attributes_array']['typeof']);
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
  * @param $vars
 *   An array of variables to pass to the theme template.
 * @return
 *   A string containing the breadcrumb output.
 */
function ndla_search_theme_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('ndla_search_theme_breadcrumb');
  if ($show_breadcrumb || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('ndla_search_theme_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('ndla_search_theme_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('ndla_search_theme_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $title = check_plain($item['title']);
        }
        else {
          $title = drupal_get_title();
        }
        if ($title) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('ndla_search_theme_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users. Make the heading invisible with .element-invisible.
      $heading = '<span>' . t('You are here: ') . '</span>';

      return '<nav id="breadcrumb" class="clearfix" role="navigation">' . $heading . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</nav>';
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Preprocess variables for region.tpl.php
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
function ndla_search_theme_preprocess_region(&$vars, $hook) {
  // Sidebar regions get some extra classes and a common template suggestion.
  if (strpos($vars['region'], 'sidebar_') === 0) {
    $vars['classes_array'][] = 'sidebar';
    // Allow a region-specific template to override Cerpus's region--sidebar.
    array_unshift($vars['theme_hook_suggestions'], 'region__sidebar');
  }
  // Use a template with no wrapper for the content, header & navigation region.
  elseif ($vars['region'] == 'content' || $vars['region'] == 'header' || $vars['region'] == 'navigation' || $vars['region'] == 'topnavigation') {
    // Allow a region-specific template to override Cerpus's region--no-wrapper.
    array_unshift($vars['theme_hook_suggestions'], 'region__no_wrapper');
  }
}

/**
 * Implements hook_page_alter().
 *
 * Look for the last block in the region. This is impossible to determine from
 * within a preprocess_block function.
 *
 * @param $page
 *   Nested array of renderable elements that make up the page.
 */
function ndla_search_theme_page_alter(&$page) {
  // Look in each visible region for blocks.
  foreach (system_region_list($GLOBALS['theme'], REGIONS_VISIBLE) as $region => $name) {
    if (!empty($page[$region])) {
      // Find the last block in the region.
      $blocks = array_reverse(element_children($page[$region]));
      while ($blocks && !isset($page[$region][$blocks[0]]['#block'])) {
        array_shift($blocks);
      }
      if ($blocks) {
        $page[$region][$blocks[0]]['#block']->last_in_region = TRUE;
      }
    }
  }
}

// Add some cool text to the search block form
function ndla_search_theme_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    //$form['search_block_form']['#type'] = 'search'; <--- u wish... see ndla_search_theme_preprocess_search_block_form
    // HTML5 placeholder attribute
    $form['search_block_form']['#attributes']['placeholder'] = t('Search..');
  }
}

/**
 * Changes the search form to use the "search" input element of HTML5.
 */
function ndla_search_theme_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
} 

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 * 	The string
 * @return
 * 	The converted string
 */	
function ndla_search_theme_id_safe($string) {
  if(empty($string))
    return $string;
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $vars
 *  An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *  A themed HTML string.
 *
 * @ingroup themeable
 * 
 */
function ndla_search_theme_menu_link(array $vars) {
  $element = $vars['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $element['#localized_options']['html'] = TRUE;
  
  $output = l('<span>'.$element['#title'].'</span>', $element['#href'], $element['#localized_options']);

  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = ndla_search_theme_id_safe($element['#title']);

  // Adding a class depending on the ID of the link (constant)
  $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override or insert variables into theme_menu_local_task().
 */
function ndla_search_theme_preprocess_menu_local_task(&$vars) {
  $link =& $vars['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab">' . $link['title'] . '</span>';
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function ndla_search_theme_menu_local_tasks(&$vars) {  
  $output = '';

  if (!empty($vars['primary'])) {
    $vars['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $vars['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $vars['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($vars['primary']);
  }
  if (!empty($vars['secondary'])) {
    $vars['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $vars['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $vars['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($vars['secondary']);
  }

  return $output;
}

/**
 * Add or remove stuff from search results.
 *
 * @param $vars
 *  An associative array containing:
 * 
 */
function ndla_search_theme_preprocess_search_result(&$vars) {
  global $language;
  $result = $vars['result'];
  $vars['url'] = check_url($result['link']);
  $vars['title'] = check_plain($result['title']);
  if (isset($result['language']) && $result['language'] != $language->language && $result['language'] != LANGUAGE_NONE) {
    $vars['title_attributes_array']['xml:lang'] = $result['language'];
    $vars['content_attributes_array']['xml:lang'] = $result['language'];
  }

  $info = array();
  if (!empty($result['module'])) {
    $info['module'] = check_plain($result['module']);
  }
  if (!empty($result['user'])) {
    $info['user'] = $result['user'];
    if (theme_get_setting('ndla_search_theme_search_result_user') == '0') {
      unset($info['user']);  
    }
  }
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'short');
    if (theme_get_setting('ndla_search_theme_search_result_date') == '0') {
      unset($info['date']);  
    }
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    
    if (theme_get_setting('ndla_search_theme_search_result_extra') == '0') {
      unset($result['extra']);  
    } else {
      $info = array_merge($info, $result['extra']);
    }

  }

  // Check for existence. User search does not include snippets.
  $vars['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';

  // Provide separated and grouped meta information..
  $vars['info_split'] = $info;
  $vars['info'] = implode(' - ', $info);
  $vars['theme_hook_suggestions'][] = 'search_result__' . $vars['module'];

}

function _ndla_search_theme_create_ie_class_names($ua_array) {
  $classes_to_use = array();
  $class_names = variable_get('ndla_search_theme_ie_class_names', array(
    'lt-ie10' => 10,
    'lt-ie9' => 9,
    'lt-ie8' => 8,
    'lt-ie7' => 7,
    'ie6' => 6,
  ));
  if($ua_array['browser'] == 'msie') {
    foreach($class_names as $name => $version) {
      if($ua_array['version'] < $version) {
        $classes_to_use[] = $name;
      }
    }
  }
  return $classes_to_use;
}


function _ndla_search_theme_identify_useragent($ua_string = null) {
  if(empty($ua_string))
    return;
  $cache = cache_get('ndla_search_theme_user_agent_'.md5($ua_string));
  if($cache) {
    return $cache->data;
  }

  $name = null; $version = null;
  $supported_browsers = variable_get('ndla_search_theme_supported_browsers', array(
    'Opera',
    'Chrome',
    'Firefox',
    'MSIE',
    'Safari',
  ));
  $regexp = "/(?:Opera[\/|\s][\d.]+)|(?:Firefox)\/(?:[\d.])+.|(?:Chrome)\/(?:[\d.])+.|(?:MSIE)\s(?:[\d.]+)|(?:Version|Safari)\/(?:[\d.]+)/";
  $regexp = variable_get('ndla_search_theme_browser_regexp', $regexp);
  preg_match_all($regexp, $ua_string, $matches);
  $name_and_version =  implode(' ', $matches[0]);

  foreach($supported_browsers as $browser_name) {
    if(stripos($name_and_version, $browser_name) !== FALSE) {
      $name = $browser_name;
      break;
    }
  }
  $match_name = ($name == "Safari") ? "Version" : $name;
  preg_match_all("/$match_name(?:[\s|\/])([\d]+)/", $name_and_version, $version_matches);

  $browser_data = array('browser' => strtolower($name), 'version' => strtolower($version_matches[1][0]));

  cache_set('ndla_search_theme_user_agent_'.md5($ua_string), $browser_data);
  return $browser_data;
}
