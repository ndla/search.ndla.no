<?php
/**
 * @brief
 *  Create, read, update, delete functions for the NDLA Search module
 */
 
function ndla_search_get_host($id) {
  static $hosts = array();
  if(empty($hosts[$id])) {
    $query = db_select('ndla_search', 'ns')
      ->condition('ns.id', $id, '=')
      ->fields('ns', array('id', 'site', 'host', 'is_drupal', 'service_url', 'enabled', 'data'));
    
    $host = $query->execute()->fetchObject();
    if($host) {
      $hosts[$id] = $host;
    }
  }
  
  return !empty($hosts[$id]) ? $hosts[$id] : NULL;
}

function ndla_set_save_host($host_data) {
  $host = (array)ndla_search_get_host($host_data['id']);
  if(!empty($host['id'])) {
    _ndla_search_update_host($host_data);
  }
  else {
    _ndla_search_insert_host($host_data);
  }
}

function _ndla_search_insert_host($host_data) {
  $id = $host_data['id'];
  $site = $host_data['site'];
  $host = $host_data['host'];
  $enabled = $host_data['enabled'];
  $is_drupal = $host_data['is_drupal'];
  $data = serialize($host_data['data']);

  db_insert('ndla_search')
    ->fields(array(
      'id' => $id,
      'site' => $site,
      'host' => $host,
      'enabled' => $enabled,
      'is_drupal' => $is_drupal,
      'data' => $data,
    ))
    ->execute();
}

function _ndla_search_update_host($host) {
  if(empty($host['data'])) {
    $host['data'] = array();
  }
  
  if(empty($host['old_id'])) {
    $host['old_id'] = $host['id'];
  }
  
  db_update('ndla_search')->fields(array(
    'id' => $host['id'],
    'site' => $host['site'],
    'host' => $host['host'],
    'enabled' => $host['enabled'],
    'is_drupal' => $host['is_drupal'],
    'service_url' => $host['service_url'],
    'data' => serialize($host['data']),
  ))
  ->condition('id', $host['old_id'], '=')
  ->execute();
}

function ndla_search_get_searchable_sites($only_enabled = FALSE) {
  $query = db_select('ndla_search', 'ns');
  $query->fields('ns', array('id', 'site', 'host', 'is_drupal', 'enabled', 'service_url', 'data'));
  $query->orderBy('site', 'ASC');

  $results = $query->execute();

  $rows = array();
  foreach($results as $result) {
    if($only_enabled && $result->enabled) {
      $rows[$result->id] = $result;
    }
    else if(!$only_enabled) {
      $rows[$result->id] = $result;
    }
  }
  
  return $rows;
}

function ndla_search_delete_site($site_id) {
  global $user;
  
  db_delete('ndla_search')
  ->condition('id', $site_id)
  ->execute();
  
  watchdog('ndla_search', 'User @name deleted site with id @site_id', array('@name' => $user->name, '@site_id' => $site_id), WATCHDOG_INFO);
}