<?php

function ndla_search_search_page() {
  $meta = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'robots',
      'content' => 'noindex, nofollow',
    ),
  );

  if(!drupal_is_front_page()) {
    drupal_add_html_head($meta, 'robots');
  }

  $query = drupal_get_query_parameters();
  $remove = array('form_build_id', 'form_id', 'op');
  $tmp = $query;
  $do_redir = FALSE;
  foreach($tmp as $index => $dummy) {
    if(in_array($index, $remove)) {
      unset($query[$index]);
      $do_redir = TRUE;
    }
  }
  if($do_redir) {
    $url = implode("/", arg());
    //drupal_goto($url, array('query' => $query));
  }
  
  $type = FALSE;
  /* Define valid responses */
  $valid_types = array('json');
  $args = arg();
  if(!empty($args[1])) {
    $type = check_plain($args[1]);
    //Someone is requesting something which we dont understand
    if(!in_array($type, $valid_types)) {
      die(theme('ndla_search_bad_request'));
    }
  }
  $form = drupal_get_form('ndla_search_search_form');
  $out = drupal_render($form);
  $limit = !empty($_GET['limit'])? $_GET['limit'] : 10; //This equals $limit*ENABLED_HOSTS in reality
  $sites = ndla_search_get_searchable_sites(TRUE);
  $num_sites = count($sites);
  //If number of sites starts to grow, be dynamic.
  //if($num_sites > 3) {
  //  $limit = 30;
  //  $limit = ceil($limit/$num_sites);
  //}
  $current_page = (!empty($_GET['page'])) ? (int)$_GET['page'] : 0;
  $offset = ($current_page > 0) ? ($current_page*$limit) : 0;
  ndla_search_populate_get_language();
  $results = ndla_search_get_results($offset, $limit, $type);

  //Normal HTML response.
  if(!$type) {
    drupal_add_js(drupal_get_path('module', 'ndla_search') . '/js/facet_checkbox_selection.js', 'file');
    drupal_add_js(drupal_get_path('module', 'ndla_search') . '/js/block_collapse.js', 'file');
    drupal_add_css(drupal_get_path('module', 'ndla_search') . '/css/ndla_collapsible.css', 'file');
    drupal_add_js(drupal_get_path('module', 'ndla_search') . '/js/drupal_autocomplete_overrides.js', 'file');

    if($results !== NULL && is_string($results)) {
      $out .= $results;
    }
    else if($results !== NULL && $results['total'] > 0) {
      pager_default_initialize($results['total'], $limit);
      $pager = theme('pager', array('quantity' => 10));
      $out .= '<div class="results-count">' . $results['total'] . ' ' . t('results') . "</div>";
      $out .= theme('item_list', array('items' => $results['rows']));
      $out .= $pager;
    }
    else if($results !== NULL) {
      $out .= t('No results.');
    }

    return $out;
  }
  else if($type == 'json') {
    print drupal_json_encode($results);
    exit();
  }
}

function ndla_search_search_form() {
  if(isset($_GET['old_query'])) {
    if(!empty($_GET['keys'])) {
      $location = current_path() . "?keys=" . check_plain($_GET['keys']) . "&" . $_GET['old_query'];
       header('Location: ' . $location);
       drupal_exit($location);
    }
  }
  $query = array();
  $old_keys = '';
  if(!empty($query['keys'])) {
    $old_keys = $query['keys'];
  }
  
  //We dont care for active filters execept language. Subject to change.
  $query_tmp = $query = drupal_get_query_parameters();
  foreach($query_tmp as $index => $dummy) {
    if($index != 'language' && $index != 'lang') {
      unset($query[$index]);
    }
  }
  if(!empty($_GET['site'])) {
    $query['site'] = $_GET['site'];
  }
  
  global $language;
  $url = url("sites/all/modules/ndla_search/ndla_search.ac.php/" . serialize($query), array('absolute' => TRUE));
  $url = str_replace("/" . $language->language . "/", "/", $url);
  $form = array(
    '#method' => 'GET',
    'keys' => array(
      '#type' => 'textfield',
      '#title' => t('Keyword'),
      '#default_value' => $old_keys,
      '#autocomplete_path' =>  $url,
    ),
    'url' => array(
      '#type' => 'value',
      '#value' => current_path(),
    ),
    'button' => array(
      '#type' => 'submit',
      '#value' => t('Search'),
      '#submit' => array('ndla_search_search_form_submit'),
    ),
  );

  if(empty($query)) {
    $form['language'] = array('#tree' => TRUE);
    $form['language'][] = array(
      '#type' => 'hidden',
      '#value' => 'en',
    );
    $form['language'][] = array(
      '#type' => 'hidden',
      '#value' => 'nb',
    );
    $form['language'][] = array(
      '#type' => 'hidden',
      '#value' => 'und',
    );
  }

  $query = drupal_get_query_parameters();
  unset($query['keys']);
  unset($query['form_build_id']);
  unset($query['form_token']);
  unset($query['form_id']);
  unset($query['op']);
  
  $flat_query = drupal_http_build_query($query);

  $form['old_query'] = array(
    '#type' => 'hidden',
    '#value' => $flat_query,
  );

  return $form;
}

