<?php

function ndla_search_admin_page() {
  $sites = ndla_search_get_searchable_sites();
  return theme('ndla_search_admin_display', array('sites' => $sites));
}

function ndla_search_admin_grep_form() {
  $form = array();

  $form['service'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    /*'ndla_grep_account' => array(
      '#title' => t('Account'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_account', ''),
      '#description' => t('The name of the user account, i.e. ":fyr" or ":ndla".', array(':fyr' => 'fyr', ':ndla' => 'ndla')),
    ),*/
    'ndla_grep_version' => array(
      '#title' => t('Version'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_version', ''),
      '#description' => t('The UDIR import version. If empty the latest version will be used.'),
    ),
    'ndla_grep_host' => array(
      '#title' => t('Host'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_host', ''),
      '#description' => t('The GREP server host name, i.e. ":url".', array(':url' => 'mycurriculum.test.ndlap3.seria.net')),
    ),
    'ndla_grep_ssl' => array(
      '#title' => t('Use SSL'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('ndla_grep_ssl', FALSE),
    ),
    'ndla_grep_username' => array(
      '#title' => t('Username'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_username', ''),
      '#description' => t('The server authentication username.'),
    ),
    'ndla_grep_password' => array(
      '#title' => t('Password'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_password', ''),
      '#description' => t('The server authentication password.'),
    ),

    'ndla_grep_timeout' => array(
      '#title' => t('Timeout'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_timeout', 30),
    ),
    'ndla_grep_wakeup' => array(
      '#title' => t('Wakeup time'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_wakeup', 5),
      '#description' => t('Graceful wakeup time. In minutes.'),
    ),
  );

  return system_settings_form($form);
}

/**
 * Add site form. Located at admin/config/serch/ndla_serch/add
 */
function ndla_search_admin_add_site_form() {
  $form['identifier'] = array(
    '#title' => t('Name'),
    '#description' => t('The name must be unique'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  
  $form['host'] = array(
    '#title' => t('Full URL to the SolR index'),
    '#description' => t('For example http://localhost:8983/solr'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  
  $form['is_drupal'] = array(
    '#title' => t('Is this site running Drupal?'),
    '#description' => t('If checked we will assume the existance of certain fields'),
    '#type' => 'checkbox',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save site'),
  );
  
  return $form;
}

function ndla_search_admin_add_site_form_validate($form, &$form_state) {
  $name = check_plain($form_state['values']['identifier']);
  $identifier = _ndla_search_name_to_id($name);
  $host = check_plain($form_state['values']['host']);
  $is_drupal = check_plain($form_state['values']['is_drupal']);

  if(!ndla_search_ping_url($host, TRUE)) {
    form_set_error('host', t('Unable to contact the host'));
  }
}

function ndla_search_admin_add_site_form_submit($form, &$form_state) {
  $host['site'] = check_plain($form_state['values']['identifier']);
  $host['id'] = $identifier = _ndla_search_name_to_id($host['site']);
  $host['host'] = check_plain($form_state['values']['host']);
  $host['is_drupal'] = check_plain($form_state['values']['is_drupal']);
  $host['data'] = array();
  $host['enabled'] = 0;
  $tmp_id = $host['id'];
  $counter = 1;
  //Increment the id if it already exists'
  while($host_tmp = ndla_search_get_host($tmp_id)) {
    $tmp_id = $tmp_id . "-" . $counter++;
  }
  $host['id'] = $tmp_id;
  ndla_set_save_host($host);
  
  drupal_set_message(t('Host saved'));  
}

function ndla_search_admin_delete_site_form($form, $form_state, $site_id) {
  $site_id = check_plain($site_id);
  $host = ndla_search_get_host($site_id);
  if(empty($host)) {
    drupal_goto('admin/config/search/ndla_search');
  }
  
  $form['site_id'] = array(
    '#type' => 'value',
    '#value' => $site_id,
  );
  
  return confirm_form($form,
      	t('Are you sure you want to delete @site?', array('@site' => $host->site)),
      	isset($_GET['destination']) ? $_GET['destination'] : "admin/config/search/ndla_search",
      	t('This action cannot be undone.'),
      	t('Delete'),
      	t('Cancel'));
}

function ndla_search_admin_delete_site_form_submit($form, &$form_state) {
  $site_id = check_plain($form_state['values']['site_id']);
  ndla_search_delete_site($site_id);
}

function ndla_search_admin_variable_form($form, $form_state) {
  $form['relate_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Relate configuration'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#tree' => FALSE,
  );
  
  $form['relate_group']['ndla_search_relate_service_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Relate service URL'),
    '#default_value' => variable_get('ndla_search_relate_service_url', 'http://relate.test.ndla.no/services/xmlrpc'),
  );

  return system_settings_form($form);
}

function ndla_search_admin_edit_site_form($form, $form_state, $site_id) {
  $site_id = check_plain($site_id);
  $host = ndla_search_get_host($site_id);
  if($host) {
    $form = array(
      'site' => array(
        '#title' => t('Name'),
        '#type' => 'textfield',
        '#default_value' => $host->site,
        '#required' => TRUE,
      ),
      
      'old_id' => array(
        '#type' => 'hidden',
        '#value' => $host->id,
      ),
      
      'id' => array(
        '#type' => 'textfield',
        '#default_value' => $host->id,
        '#title' => t('Database ID'),
        '#required' => TRUE,
        '#description' => t('Do NOT change this for deling, ndla, nygiv (FYR) and topics'),
      ),
      
      'host' => array(
        '#title' => t('Full URL to the SolR index'),
        '#description' => t('For example http://localhost:8983/solr'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => $host->host,
      ),
      
      'service_url' => array(
        '#title' => t('NDLA Service URL'),
        '#type' => 'textfield',
        '#default_value' => $host->service_url,
        '#description' => t('The menu callback which gives us the SolR config. Ie) http://ndla.no/ndla_service/solr/config'),
      ),
      
      'is_drupal' => array(
        '#title' => t('Is this site running Drupal?'),
        '#description' => t('If checked we will assume the existance of certain fields'),
        '#type' => 'checkbox',
        '#default_value' => $host->is_drupal,
      ),
      
      'enabled' => array(
        '#title' => t('Enabled'),
        '#description' => t('If enabled this host will go live'),
        '#default_value' => $host->enabled,
        '#type' => 'checkbox',
      ),
      
      'buttons' => array(
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Save'),
        ),
      ),
    );
    
    return $form;
  }
  //Someone is giving us a id which does not exist.
  else {
    drupal_set_message(t('Host not found in database'), 'warning');
    drupal_goto('admin/config/search/ndla_search');
  }
}

function ndla_search_admin_edit_site_form_validate($form, &$form_state) {
  $values = array(
    'id' => NULL,
    'site' => NULL,
    'host' => NULL,
    'service_url' => NULL,
    'is_drupal' => NULL,
    'enabled' => NULL,
    'old_id' => NULL,
  );
  
  
  foreach($values as $index => $dummy) {
    $$index = check_plain($form_state['values'][$index]);
  }
  
  if($old_id != $id || empty($old_id)) {
    $row = db_query("SELECT id FROM {ndla_search} WHERE id = :id", array(':id' => check_plain($id)))->fetchObject();
    if(!empty($row->id)) {
      form_set_error('id', t('A host with this ID already exist.'));
    }
  }

  if(!ndla_search_ping_url($host, TRUE)) {
    form_set_error('host', t('Unable to contact the host'));
  }
  if(!empty($service_url) && !ndla_search_get_solr_config($service_url)) {
    form_set_error('service_url', t('Service unavailable'));
  }
}

function ndla_search_admin_edit_site_form_submit($form, &$form_state) {
  $values = array(
    'id' => NULL,
    'site' => NULL,
    'host' => NULL,
    'service_url' => NULL,
    'is_drupal' => NULL,
    'enabled' => NULL,
    'old_id' => NULL,
  );
  
  foreach($values as $index => $dummy) {
    $values[$index] = check_plain($form_state['values'][$index]);
  }
  
  if(!empty($values['service_url'])) {
    if($data = ndla_search_get_solr_config($values['service_url'])) {
      $values['data'] = $data;
    }
  }
  
  if($form_state['values']['op'] == t('Save')) {
    _ndla_search_update_host($values);
    drupal_set_message(t('Site updated'));
    if($values['old_id'] != $values['id']) {
      drupal_goto('admin/config/search/ndla_search/edit/' . $values['id']);
    }
  }
}

